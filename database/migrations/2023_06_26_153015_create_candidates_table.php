<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->id();
            $table->string('fname');
            $table->string('lname');
            $table->string('linkedin', 200)->nullable();
            $table->string('client');
            $table->string('position');
            $table->string('seniority');
            $table->string('salary')->nullable();
            $table->string('start_date')->nullable();
            $table->string('date_contacted');
            $table->longText('comments')->nullable();
            $table->string('attach', 200)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
}
