<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_group')->insert([
            'name' => 'administrator',
        ]);

        DB::table('user_group')->insert([
            'name' => 'user',
        ]);

        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@test.me',
            'password' => Hash::make('Test1234'),
            'group_id' => 1,
        ]);

        DB::table('users')->insert([
            'name' => 'user',
            'email' => 'user@test.me',
            'password' => Hash::make('Test1234'),
            'group_id' => 2,
        ]);
    }
}
