# hrtool



## System requirements - Laravel 8

- PHP >= 7.3 (**PHP 8.x strongly recommended**)
- BCMath PHP Extension
- Ctype PHP Extension
- Fileinfo PHP Extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- GIT
- Node.js
- Composer
- MySQL
- Apache or Nginx

## Deploy

- Pull files to desired folder on server
- Create mysql database on server
- Point "public" subfolder of that folder to desired domain or subdomain
- Go to that folder via terminal
- Create or edit .env file - use .env.example end edit database parameters (DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, DB_PASSWORD) and APP_URL
- Execute "composer install"
- Execute "composer update"
- Execute "php artisan migrate:fresh --seed --seeder=UserSeeder"
- Execute "npm install"
- Execute "npm run dev" or "npm run production"

## Automatically created users

- Administrator - admin@test.me / Test1234
- User - user@test.me / Test1234
