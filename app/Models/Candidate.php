<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'fname',
        'lname',
        'email',
        'phone',
        'linkedin',
        'client',
        'position',
        'seniority',
        'salary',
        'start_date',
        'date_contacted',
        'comments',
        'attach',
    ];
}
