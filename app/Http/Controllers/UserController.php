<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\View\View;
use DataTables;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function show(Request $request)
    {
        if ($request->ajax()) {
            $data = User::select('*');
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="'.url('/').'/edit-user/'.$row->id.'" class="edit btn btn-primary btn-sm">View/Edit</a>';
                        $btn .= ' <a href="'.url('/').'/delete-user/'.$row->id.'" class="remove btn btn-danger btn-sm">Remove</a>';
                        return $btn;
                    })
                    ->addColumn('group', function($row){
                        if(isset($row->group)){
                            return $row->group->name;
                        }else{
                            return '';
                        }

                    })
                    ->rawColumns(['action', 'group'])
                    ->make(true);
        }

        return view('users');
    }

    public function create(Request $request)
    {
        $request->validate([
            'fname' => ['required', 'string', 'max:191'],
            'lname' => ['required', 'string', 'max:191'],
            'email' => ['required', 'string', 'email', 'max:191', 'unique:users'],
            'password' => ['required', Rules\Password::defaults()],
        ]);

        $user = User::create([
            'name' => $request->fname,
            'lname' => $request->lname,
            'group_id' => $request->group,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return redirect()->back()->with('user-added', 'User created!');
    }

    public function delete(int $id)
    {
        $user = User::find($id);
        $user->forceDelete();
        return redirect()->back()->with('user-deleted', 'User deleted!');
    }

    public function view(int $id)
    {
        $user = User::find($id);
        return view('view-user')->with([
            'user' => $user,
        ]);
    }

    public function update(int $id, Request $request)
    {
        $data = [
            'name' => $request->fname,
            'lname' => $request->lname,
            'group_id' => $request->group,
            'email' => $request->email,
        ];

        if(isset($request->password) && $request->password != ''){
            $data['password'] = Hash::make($request->password);
        }
        $user = User::where('id', $id)->update($data);
        return redirect()->back()->with('user-modified', 'User updated!');
    }
}
