<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Candidate;
use Illuminate\View\View;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\DB;

class CandidateController extends Controller
{
    public function show(Request $request)
    {
        if ($request->ajax()) {
            $data = Candidate::select('*');
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="'.url('/').'/edit-candidate/'.$row->id.'" class="edit btn btn-primary btn-sm">View/Edit</a>';
                        $btn .= ' <a href="'.url('/').'/delete-candidate/'.$row->id.'" class="remove btn btn-danger btn-sm">Remove</a>';
                        return $btn;
                    })
                    ->addColumn('comms', function($row){
                       // $btn = '<a href="'.url('/').'/storage/app/public/cv/'.$row->attach.'" class="btn btn-primary btn-sm">CV</a>';
                        if(isset($row->comments) && $row->comments != '' ){
                            $comments = json_decode($row->comments, true);
                            $html = '';
                            foreach($comments as $c){
                                $html .= $c . '<br>******<br>';
                            }
                        }else{
                            $html = '';
                        }

                        return $html;
                    })
                    ->addColumn('cv', function($row){
                        // $btn = '<a href="'.url('/').'/storage/app/public/cv/'.$row->attach.'" class="btn btn-primary btn-sm">CV</a>';
                         if(isset($row->attach) && $row->attach != '' ){
                             $btn = '<a href="'.\Storage::disk('public')->url('/app/public/cv/'.$row->attach).'">CV</a>';
                         }else{
                             $btn = '';
                         }

                         return $btn;
                     })
                    ->rawColumns(['action','cv', 'comms'])
                    ->make(true);
        }

        return view('candidates');
    }

    public function create(Request $request)
    {
        $request->validate([
            'fname' => ['required', 'string', 'max:191'],
            'lname' => ['required', 'string', 'max:191'],
            'email' => ['required', 'string', 'email', 'max:191'],
            'phone' => ['required', 'string', 'max:25'],
            'linkedin' => ['nullable', 'string', 'max:200'],
            'client' => ['required', 'string', 'max:191'],
            'position' => ['required', 'string', 'max:191'],
            'seniority' => ['required', 'string', 'max:25'],
            'salary' => ['nullable', 'string', 'max:191'],
            'start_date' => ['nullable', 'string', 'max:191'],
            'date_contacted' => ['required', 'string', 'max:191'],
            'comments' => ['nullable', 'string'],
            'attach' => ['nullable', 'mimes:doc,docx,pdf|max:5120']
        ]);

        $cv_name = '';
        $comments = [];

        if ($request->file('file') !== null) {
            $cv_name = time() . '_' . $request->file('file')->getClientOriginalName();
            $cv_path = $request->file('file')->storeAs('cv', $cv_name, 'public');
        }

        if (isset($request->comments) && $request->comments != ''){
            array_push($comments, $request->comments_user . ': ' . $request->comments);
        }


        $candidate = Candidate::create([
            'fname' => $request->fname,
            'lname' => $request->lname,
            'email' => $request->email,
            'phone' => $request->phone,
            'linkedin' => $request->linkedin,
            'client' => $request->client,
            'position' => $request->position,
            'seniority' => $request->seniority,
            'salary' => $request->salary,
            'start_date' => $request->start_date,
            'date_contacted' => $request->date_contacted,
            'comments' => json_encode($comments),
            'attach' => $cv_name,
        ]);

        return redirect()->back()->with('candidate-added', 'Candidate created!');
    }

    public function delete(int $id)
    {
        $candidate = Candidate::find($id);
        $candidate->forceDelete();
        return redirect()->back()->with('candidate-deleted', 'Candidate deleted!');
    }

    public function view(int $id)
    {
        $candidate = Candidate::find($id);

        if(isset($candidate->comments) && $candidate->comments != '' ){
            $comments = json_decode($candidate->comments, true);
            $coms = '';
            foreach($comments as $c){
                $coms .= $c . ' ************ ';
            }
        }else{
            $coms = '';
        }

        $seniority = ['Graduate', 'Junior', 'Medior', 'Senior', 'Team Lead'];

        if(isset($candidate->attach) && $candidate->attach != '' ){
            $cv = \Storage::disk('public')->url('/app/public/cv/'.$candidate->attach);
        }else{
            $cv = '';
        }

        return view('view-candidate')->with([
            'candidate' => $candidate,
            'candidate_comments' => $coms,
            'seniority' => $seniority,
            'cv' => $cv,
        ]);
    }

    public function update(int $id, Request $request)
    {
        $cv_name = '';
        $comments = [];

        if ($request->file('file') !== null) {
            $cv_name = time() . '_' . $request->file('file')->getClientOriginalName();
            $cv_path = $request->file('file')->storeAs('cv', $cv_name, 'public');
        }

        $existing_comments = Candidate::where('id', $id)->value('comments');
        $existing_comments = json_decode($existing_comments, true);
        if(!is_array($existing_comments)){
            $existing_comments = [];
        }

        if (isset($request->comments_new) && $request->comments_new != ''){

            array_push($existing_comments, $request->comments_user . ': ' . $request->comments_new);
        }

        $data = [
            'fname' => $request->fname,
            'lname' => $request->lname,
            'email' => $request->email,
            'phone' => $request->phone,
            'linkedin' => $request->linkedin,
            'client' => $request->client,
            'position' => $request->position,
            'seniority' => $request->seniority,
            'salary' => $request->salary,
            'start_date' => $request->start_date,
            'date_contacted' => $request->date_contacted,
            'comments' => json_encode($existing_comments),
            'attach' => $cv_name,
        ];

        $candidate = Candidate::where('id', $id)->update($data);
        return redirect()->back()->with('candidate-modified', 'Candidate updated!');
    }
}
