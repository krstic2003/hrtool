<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CandidateController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::middleware('auth')->group(function () {

    //users
    Route::get('/users', [UserController::class, 'show'])->name('users')->middleware(['usergroup']);
    Route::post('/users', [UserController::class, 'create'])->middleware(['usergroup']);
    Route::get('/delete-user/{id}', [UserController::class, 'delete'])->name('delete-user')->middleware(['usergroup']);
    Route::get('/edit-user/{id}', [UserController::class, 'view'])->name('view-user')->middleware(['usergroup']);
    Route::post('/edit-user/{id}', [UserController::class, 'update'])->middleware(['usergroup']);

    //candidates
    Route::get('/candidates', [CandidateController::class, 'show'])->name('candidates');
    Route::post('/candidates', [CandidateController::class, 'create']);
    Route::get('/delete-candidate/{id}', [CandidateController::class, 'delete'])->name('delete-candidate');
    Route::get('/edit-candidate/{id}', [CandidateController::class, 'view'])->name('view-candidate');
    Route::post('/edit-candidate/{id}', [CandidateController::class, 'update']);
});
