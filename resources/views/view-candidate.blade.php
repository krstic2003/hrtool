<x-app-layout>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                @if(Session::has('candidate-modified'))
                    <div class="green-msg">
                        <p>{{ Session::get('candidate-modified') }}</p>
                    </div>
                @endif

                <div class="p-6 bg-white border-b border-gray-200">
                    <h2 class="section-title">{{ __('Update Candidate') }}</h2>
                    <form id="create-user-form" action="{{url("/edit-candidate/" . $candidate->id )}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="fname">First Name*</label>
                            <input type="text" id="fname" name="fname" class="form-control" placeholder="First Name" minlength="2" required value="{{ $candidate->fname }}">
                        </div>
                        <div class="form-group">
                            <label for="lname">Last Name*</label>
                            <input type="text" id="lname" name="lname" class="form-control" placeholder="Last Name" minlength="2" required value="{{ $candidate->lname }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email*</label>
                            <input type="text" id="email" name="email" class="form-control" placeholder="Email" required value="{{ $candidate->email }}">
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone*</label>
                            <input type="text" id="phone" name="phone" class="form-control" placeholder="Phone" required value="{{ $candidate->phone }}">
                        </div>
                        <div class="form-group">
                            <label for="linkedin">LinkedIn</label>
                            <input type="text" id="linkedin" name="linkedin" class="form-control" placeholder="LinkedIn" value="{{ $candidate->linkedin }}">
                        </div>
                        <div class="form-group">
                            <label for="client">Client*</label>
                            <input type="text" id="client" name="client" class="form-control" placeholder="Client" required value="{{ $candidate->client }}">
                        </div>
                        <div class="form-group">
                            <label for="position">Position*</label>
                            <input type="text" id="position" name="position" class="form-control" placeholder="Position" required value="{{ $candidate->position }}">
                        </div>
                        <div class="form-group">
                            <label for="seniority">Seniority*</label>
                            <select name="seniority" id="seniority" class="form-control">
                                @foreach ($seniority as $s)
                                    @if ($s == $candidate->seniority)
                                        <option value="{{ $s }}" selected>{{ $s }}</option>
                                    @else
                                        <option value="{{ $s }}">{{ $s }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="salary">Salary (EUR)</label>
                            <input type="text" id="salary" name="salary" class="form-control" placeholder="Salary" value="{{ $candidate->salary }}">
                        </div>
                        <div class="form-group datepicker">
                            <label for="start_date">Start date</label>
                            <input type="text" id="start_date" name="start_date" class="form-control " placeholder="DD/MM/YYYY" value="{{ $candidate->start_date }}">
                        </div>
                        <div class="form-group datepicker">
                            <label for="date_contacted">Date contacted*</label>
                            <input type="text" id="date_contacted" name="date_contacted" class="form-control" placeholder="DD/MM/YYYY" required value="{{ $candidate->date_contacted }}">
                        </div>
                        <div class="form-group">
                            <label for="comments">Add comment</label>
                            <input type="hidden" name="comments_user" value="{{ Auth::user()->name }} {{ Auth::user()->lname }}">
                            <input type="text" name="comments_new">
                            <textarea name="comments" id="comments" cols="28" rows="4" class="form-control" disabled>Existing comments: {{ $candidate_comments }}</textarea>
                        </div>
                        <div class="form-group" style="width: 40%">
                            <label for="file">Upload CV (doc, docx, pdf - max: 5mb)</label>
                            <input type="file" id="file" name="file" class="form-control form-control-file" placeholder="Upload CV">
                            <a class="existing-cv" href="{{ $cv }}">Existing CV</a>

                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                        <a href="{{url("/candidates/")}}" class="btn btn-warning">Back to all candidates</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $("#create-user-form").validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                }
            });

            $('#start_date').datepicker({
                autoclose: true,
                format: "dd/mm/yyyy"
            });

            $('#date_contacted').datepicker({
                autoclose: true,
                format: "dd/mm/yyyy"
            });

        });
      </script>
</x-app-layout>

