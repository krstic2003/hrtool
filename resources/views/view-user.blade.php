<x-app-layout>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                @if(Session::has('user-modified'))
                    <div class="green-msg">
                        <p>{{ Session::get('user-modified') }}</p>
                    </div>
                @endif

                <div class="p-6 bg-white border-b border-gray-200">
                    <h2 class="section-title">{{ __('Update User') }}</h2>
                    <form id="create-user-form" action="{{url("/edit-user/" . $user->id )}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="fname">First Name*</label>
                            <input type="text" id="fname" name="fname" class="form-control" placeholder="First Name" minlength="2" required value="{{ $user->name }}">
                        </div>
                        <div class="form-group">
                            <label for="lname">Last Name*</label>
                            <input type="text" id="lname" name="lname" class="form-control" placeholder="Last Name" minlength="2" required value="{{ $user->lname }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email*</label>
                            <input type="text" id="email" name="email" class="form-control" placeholder="Email" required value="{{ $user->email }}">
                        </div>
                        <div class="form-group">
                            <label for="group">Group*</label>
                            <select name="group" id="group" class="form-control">
                                @if ($user->group_id == 1)
                                    <option value="2">User</option>
                                    <option value="1" selected>Administrator</option>
                                @else
                                    <option value="2" selected>User</option>
                                    <option value="1">Administrator</option>
                                @endif

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="password">Password*</label>
                            <input type="password" id="password" name="password" class="form-control" placeholder="Password"  >
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                        <a href="{{url("/users/")}}" class="btn btn-warning">Back to all users</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $("#create-user-form").validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                }
            });

        });
      </script>
</x-app-layout>

