<x-app-layout>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                @if(Session::has('candidate-deleted'))
                    <div class="red-msg" style="background-color: red; padding: 5px; color: white;">
                        <p>{{ Session::get('candidate-deleted') }}</p>
                    </div>
                @endif


                @if(Session::has('candidate-added'))
                    <div class="green-msg">
                        <p>{{ Session::get('candidate-added') }}</p>
                    </div>
                @endif

                <div class="p-6 bg-white border-b border-gray-200">
                    <h2 class="section-title">{{ __('Create Candidate') }}</h2>
                    <form id="create-user-form" action="{{url()->current()}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="fname">First Name*</label>
                            <input type="text" id="fname" name="fname" class="form-control" placeholder="First Name" minlength="2" required>
                        </div>
                        <div class="form-group">
                            <label for="lname">Last Name*</label>
                            <input type="text" id="lname" name="lname" class="form-control" placeholder="Last Name" minlength="2" required>
                        </div>
                        <div class="form-group">
                            <label for="email">Email*</label>
                            <input type="text" id="email" name="email" class="form-control" placeholder="Email" required>
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone*</label>
                            <input type="text" id="phone" name="phone" class="form-control" placeholder="Phone" required>
                        </div>
                        <div class="form-group">
                            <label for="linkedin">LinkedIn</label>
                            <input type="text" id="linkedin" name="linkedin" class="form-control" placeholder="LinkedIn">
                        </div>
                        <div class="form-group">
                            <label for="client">Client*</label>
                            <input type="text" id="client" name="client" class="form-control" placeholder="Client" required>
                        </div>
                        <div class="form-group">
                            <label for="position">Position*</label>
                            <input type="text" id="position" name="position" class="form-control" placeholder="Position" required>
                        </div>
                        <div class="form-group">
                            <label for="seniority">Seniority*</label>
                            <select name="seniority" id="seniority" class="form-control">
                                <option value="Graduate">Graduate</option>
                                <option value="Junior">Junior</option>
                                <option value="Medior">Medior</option>
                                <option value="Senior">Senior</option>
                                <option value="Team Lead">Team Lead</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="salary">Salary (EUR)</label>
                            <input type="text" id="salary" name="salary" class="form-control" placeholder="Salary">
                        </div>
                        <div class="form-group datepicker">
                            <label for="start_date">Start date</label>
                            <input type="text" id="start_date" name="start_date" class="form-control " placeholder="DD/MM/YYYY">
                        </div>
                        <div class="form-group datepicker">
                            <label for="date_contacted">Date contacted*</label>
                            <input type="text" id="date_contacted" name="date_contacted" class="form-control" placeholder="DD/MM/YYYY" required>
                        </div>
                        <div class="form-group">
                            <label for="comments">Add comment</label>
                            <input type="hidden" name="comments_user" value="{{ Auth::user()->name }} {{ Auth::user()->lname }}">
                            <textarea name="comments" id="comments" cols="30" rows="4" class="form-control"></textarea>
                        </div>
                        <div class="form-group" style="width: 40%">
                            <label for="file">Upload CV (doc, docx, pdf - max: 5mb)</label>
                            <input type="file" id="file" name="file" class="form-control form-control-file" placeholder="Upload CV">
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                <div class="p-6 bg-white border-b border-gray-200" style="width: 100%;">
                    <h2 class="section-title">{{ __('All Candidates') }}</h2>
                    <div class="table-responsive">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th width="30px">ID</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Linkedin</th>
                                    <th>Client</th>
                                    <th>Position</th>
                                    <th>Seniority</th>
                                    <th>Salary</th>
                                    <th>Start Date</th>
                                    <th>Date Contacted</th>
                                    <th>CV</th>
                                    <th>Comments</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {

            var table = $('.data-table').DataTable({
                columnDefs: [{
                    defaultContent: "",
                    targets: "_all"
                }],
                dom: '<"top"Biflp<"clear">>rt<"bottom"ip<"clear">>',
                buttons: [
                    'colvis',
                    'csv'
                ],
                processing: true,
                serverSide: true,
                pageLength: 25,
                lengthMenu: [5,10, 25, 50, 100],
                ajax: "{{ route('candidates') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'fname', name: 'fname'},
                    {data: 'lname', name: 'lname'},
                    {data: 'email', name: 'email'},
                    {data: 'phone', name: 'phone'},
                    {data: 'linkedin', name: 'linkedin'},
                    {data: 'client', name: 'client'},
                    {data: 'position', name: 'position'},
                    {data: 'seniority', name: 'seniority'},
                    {data: 'salary', name: 'salary'},
                    {data: 'start_date', name: 'start_date'},
                    {data: 'date_contacted', name: 'date_contacted'},
                    {data: 'cv', name: 'cv', orderable: false, searchable: false},
                    {data: 'comms', name: 'comms'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            $("#create-user-form").validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                }
            });

            $('#start_date').datepicker({
                autoclose: true,
                format: "dd/mm/yyyy"
            });

            $('#date_contacted').datepicker({
                autoclose: true,
                format: "dd/mm/yyyy"
            });


        });
      </script>
</x-app-layout>

