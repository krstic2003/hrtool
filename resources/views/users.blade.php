<x-app-layout>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                @if(Session::has('user-deleted'))
                    <div class="red-msg" style="background-color: red; padding: 5px; color: white;">
                        <p>{{ Session::get('user-deleted') }}</p>
                    </div>
                @endif


                @if(Session::has('user-added'))
                    <div class="green-msg">
                        <p>{{ Session::get('user-added') }}</p>
                    </div>
                @endif

                <div class="p-6 bg-white border-b border-gray-200">
                    <h2 class="section-title">{{ __('Create User') }}</h2>
                    <form id="create-user-form" action="{{url()->current()}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="fname">First Name*</label>
                            <input type="text" id="fname" name="fname" class="form-control" placeholder="First Name" minlength="2" required>
                        </div>
                        <div class="form-group">
                            <label for="lname">Last Name*</label>
                            <input type="text" id="lname" name="lname" class="form-control" placeholder="Last Name" minlength="2" required>
                        </div>
                        <div class="form-group">
                            <label for="email">Email*</label>
                            <input type="text" id="email" name="email" class="form-control" placeholder="Email" required>
                        </div>
                        <div class="form-group">
                            <label for="group">Group*</label>
                            <select name="group" id="group" class="form-control">
                                <option value="2">User</option>
                                <option value="1">Administrator</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="password">Password*</label>
                            <input type="password" id="password" name="password" class="form-control" placeholder="Password" minlength="8" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                <div class="p-6 bg-white border-b border-gray-200" >
                    <h2 class="section-title">{{ __('All Users') }}</h2>
                    <div class="table-responsive">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th width="100px">No</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Group</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {

            var table = $('.data-table').DataTable({
                columnDefs: [{
                    defaultContent: "",
                    targets: "_all"
                }],
                dom: '<"top"Biflp<"clear">>rt<"bottom"ip<"clear">>',
                buttons: [
                    'colvis',
                    'csv'
                ],
                processing: true,
                serverSide: true,
                pageLength: 25,
                lengthMenu: [5,10, 25, 50, 100],
                ajax: "{{ route('users') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'lname', name: 'lname'},
                    {data: 'email', name: 'email'},
                    {data: 'group', name: 'group'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            $("#create-user-form").validate({
                rules: {
                    password: {
                        required: true,
                        minlength: 8
                    },
                    email: {
                        required: true,
                        email: true
                    }
                }
            });

        });
      </script>
</x-app-layout>

